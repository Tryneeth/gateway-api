import { Document, Model } from 'mongoose';
import { FindAllParams } from '../dto/find-all-params.dto';
import { CountParams } from '../dto/count-params.dto';

export class MongooseRepository<T extends Document> {
  searchProperties: string[];
  constructor(readonly model: Model<T>, searchProperties?: string[]) {
    this.searchProperties = searchProperties;
  }

  async find(id): Promise<T> {
    return await this.model.findById(id);
  }

  async findAll(params: FindAllParams = {}): Promise<T[]> {
    params.filters = params.filters || {};
    if (params.search) {
      params = this.buildRegexFilters(params);
    }
    const skip = params.page * params.ipp;
    return await this.model.find(params.filters, null, {
      limit: params.ipp,
      skip,
      sort: params.sort,
    });
  }

  async count(params: CountParams): Promise<number> {
    params.filters = params.filters || {};
    if (params.search) {
      params = this.buildRegexFilters(params);
    }
    return await this.model.count(params.filters);
  }

  async save(resource): Promise<T> {
    const newResource = new this.model(resource);
    return await newResource.save();
  }

  async bulkSave(resources): Promise<T[]> {
    // @ts-ignore
    return await this.model.insertMany(resources);
  }

  async update(id, resource): Promise<T> {
    return await this.model.findByIdAndUpdate(id, resource, { new: true });
  }

  async delete(id): Promise<T> {
    return await this.model.findByIdAndRemove(id);
  }

  buildRegexFilters(params) {
    params.filters.$or = [];
    const filteredProperties = Object.keys(params.filters);
    const regexProperties = this.searchProperties.filter((searchProperty) => !filteredProperties.includes(searchProperty));
    for (const regexProperty of regexProperties) {
      params.filters.$or.push({[regexProperty]: {$regex: params.search, $options: 'mi'}});
    }
    return params;
  }
}
