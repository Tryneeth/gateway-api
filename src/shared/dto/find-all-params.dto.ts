export class FindAllParams {
  ipp?: number;
  page?: number;
  sort?: string[];
  filters?: any;
  search?: string;
}
