import { Schema, Document } from 'mongoose';

export const status = [STATUS.OFFLINE, STATUS.ONLINE];

export const enum STATUS {
  ONLINE = 'ONLINE',
  OFFLINE = 'OFFLINE',
}

export const DeviceSchema = new Schema({
  uid: {type: Number, unique: true, required: true},
  vendor: {type: String},
  createdDate: {type: Date, default: new Date()},
  status: {type: String, enum: status, default: STATUS.OFFLINE},
  gatewayId: {type: Schema.Types.ObjectId, ref: 'Gateway', required: true},
});

export interface Device extends Document {
  uid: number;
  vendor: string;
  createdDate: Date;
  status: STATUS;
  gatewayId: string;
}
