import { Schema, Document } from 'mongoose';

export const GatewaySchema = new Schema({
  serial: {type: String, required: true, unique: true},
  name: {type: String, required: true},
  ipv4: {type: String, required: true, unique: true},
});

export interface Gateway extends Document {
  serial: string;
  name: string;
  ipv4: string;
}
