import {Body, ConflictException, Controller, Delete, Get, Param, Post} from '@nestjs/common';
import {GatewayService} from '../services/gateway.service';
import {GatewayDto} from '../dto/gateway.dto';
import {Gateway} from '../schemas/gateway.schema';
import {DeviceService} from '../services/device.service';

@Controller('gateways')
export class GatewayController {
    constructor(
        private readonly gatewayService: GatewayService,
        private readonly deviceService: DeviceService,
    ) {
    }

    @Post()
    async save(@Body('resource') gateway: GatewayDto): Promise<Gateway> {
        const count = await this.gatewayService.count({filters: {serial: gateway.serial}});
        if (count > 0)
            throw new ConflictException('This Serial already exists in the database');
        else
            return await this.gatewayService.save(gateway);
    }

    @Get()
    async findAll(): Promise<any[]> {
        return this.gatewayService.adddevicesCountToGateways(await this.gatewayService.findAll());
    }

    @Get(':id')
    async find(@Param('id') id: string): Promise<Gateway> {
        return this.gatewayService.adddevicesCount(await this.gatewayService.find(id));
    }

    @Delete(':id')
    async delete(@Param('id') id: string): Promise<Gateway> {
        return await this.gatewayService.delete(id);
    }
}
