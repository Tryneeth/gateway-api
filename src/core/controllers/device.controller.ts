import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';

import { DeviceService } from '../services/device.service';
import { Device } from '../schemas/device.schema';
import { DeviceDto } from '../dto/device.dto';

@Controller('gateways/:gatewayId/devices')
export class DeviceController {
    constructor(private readonly deviceService: DeviceService) {}

  @Get()
  async findAll(@Param('gatewayId') gatewayId: string): Promise<Device[]> {
    return await this.deviceService.findAll({filters: {gatewayId}});
  }

  @Get(':id')
  async find(@Param('id') id: string): Promise<Device> {
    return await this.deviceService.find(id);
  }

  @Post()
  async save(@Body('resource') device: DeviceDto, @Param('gatewayId') gatewayId: string): Promise<Device> {
    return await this.deviceService.save({...device, gatewayId});
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<Device> {
      return await this.deviceService.delete(id);
  }

  @Get('count')
  async count(@Param('gatewayId') gatewayId: string): Promise<number> {
      return await this.deviceService.count({filters: gatewayId});
  }
}
