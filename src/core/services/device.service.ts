import { Injectable, ConflictException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { MongooseRepository } from '../../shared/repository/mongoose.repository';
import { Device } from '../schemas/device.schema';
import { MESSAGE } from '../../shared/exception-messages/exception-messages';

@Injectable()
export class DeviceService extends MongooseRepository<Device> {
  constructor(
    @InjectModel('Device') private readonly deviceModel: Model<Device>,
  ) {
    super(deviceModel);
  }

  async save(device): Promise<Device> {
    const countGatewayDevices = await this.count({filters: {gatewayId: device.gatewayId}});
    if (countGatewayDevices < 10) {
      return await super.save(device);
    } else {
      throw new ConflictException(MESSAGE.DEVICE_LIMIT);
    }
  }

  async deleteAllByGatewayId(gatewayId: string): Promise<void> {
    await this.deviceModel.deleteMany({gatewayId}).exec();
  }
}
