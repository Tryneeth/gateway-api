import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Gateway } from '../schemas/gateway.schema';
import { MongooseRepository } from '../../shared/repository/mongoose.repository';
import { DeviceService } from './device.service';

@Injectable()
export class GatewayService extends MongooseRepository<Gateway> {
  constructor(
    @InjectModel('Gateway') private readonly gatewayModel: Model<Gateway>,
    private deviceService: DeviceService,
  ) {
    super(gatewayModel);
  }

  async delete(id): Promise<Gateway> {
    const gatewayDeleted = await super.delete(id);
    this.deviceService.deleteAllByGatewayId(id);
    return gatewayDeleted;
  }

  async adddevicesCount(gateway: Gateway) {
    return {
      ...gateway.toObject(),
      devicesCount: await this.deviceService.count({filters: {gatewayId: gateway.id}})
    };
  }

  async adddevicesCountToGateways(gateways: Gateway[]) {
    const gatewaysToResponse = [];
    for (const gateway of gateways) {
      gatewaysToResponse.push(await this.adddevicesCount(gateway));
    }
    return gatewaysToResponse;
  }
}
