import {IsAlphanumeric, IsNotEmpty, IsIP} from 'class-validator';

export class GatewayDto {
  @IsAlphanumeric()
  @IsNotEmpty()
  serial: string;

  @IsAlphanumeric()
  @IsNotEmpty()
  name: string;

  @IsIP('4')
  @IsNotEmpty()
  ipv4: string;
}
