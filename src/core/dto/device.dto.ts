import { IsNotEmpty, IsIn, IsInt, IsEmpty } from 'class-validator';

import {status} from '../schemas/device.schema';

export class DeviceDto {
  @IsInt()
  uid: number;

  @IsNotEmpty()
  vendor: string;

  @IsIn(status)
  status: string;
}
