import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GatewaySchema } from './schemas/gateway.schema';
import { GatewayController } from './controllers/gateway.controller';
import { GatewayService } from './services/gateway.service';
import { DeviceSchema } from './schemas/device.schema';
import { DeviceController } from './controllers/device.controller';
import { DeviceService } from './services/device.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: 'Gateway', schema: GatewaySchema},
      {name: 'Device', schema: DeviceSchema},
    ]),
  ],
  controllers: [GatewayController, DeviceController],
  providers: [GatewayService, DeviceService],
})
export class CoreModule {}
