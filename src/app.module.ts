import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CoreModule } from './core/core.module';

@Module({
  imports: [
    MongooseModule.forRoot(process.env.MONGO_URI, {
      retryDelay: 1000,
      retryAttempts: Number.MAX_VALUE,
      useNewUrlParser: true,
      useCreateIndex: true,
    }),
    CoreModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
