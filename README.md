## Installation

Node 10 and MongoDB 4 are needed for this project

```bash
$ npm install
```

## Config

Add environment MONGO_URI with the Mongo Database server URI

```bash
$ export MONGO_URI='mongodb://localhost:27017/gateway-database'
```

## Running the app

```bash
# development
$ npm run start:dev

# production mode
$ npm run start:prod
```
